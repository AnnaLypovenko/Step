//подключение our services//
const tabsCaptionLi = document.querySelectorAll('.tabs li');

for (let i = 0; i < tabsCaptionLi.length; i++) {
    tabsCaptionLi[i].addEventListener('click', switchTab);
}

function switchTab() {
    const elemUl = this.parentElement;
    const allListItems = elemUl.children;
    const arrayListItems = Array.prototype.slice.call(allListItems);

    arrayListItems.forEach(function(item) {
        item.classList.remove('active');
    });
    this.classList.add('active');
    const tabsContent = document.querySelectorAll('.centered-content .tabs__content');
    for (let i = 0; i < tabsContent.length; i++) {
        tabsContent[i].classList.remove('active');
    }

    const position = arrayListItems.indexOf(this);

    tabsContent[position].classList.add('active');
}


//подключение галереи//

$(document).ready(function() {

    $(".item").hide();
    $(".item").slice(0,12).show();
    $(".btn-show-more").click(function(){
        let imageType = $(".btn.active").data("filter");
        imageType = imageType.replace(".","");


        /*
        <div data-type="webd" class="item webd col-3">
                <a href=""><img src="image/gallery/web-design/web-design3.jpg" height="206" width="284"/></a>
            </div>
         */

      for (let i=0; i<12; i++) {
          let div = document.createElement("div");
          div.setAttribute("data-type", imageType);
          div.setAttribute("class", `item ${imageType} col-4`);

          let a = document.createElement('a');
          a.setAttribute("href", '');
          div.appendChild(a);

          let img = document.createElement("img");
          img.setAttribute('src','image/gallery/web-design/web-design3.jpg');

          a.appendChild(img);

          let row = document.getElementsByClassName("row")[0];
          row.appendChild(div);
      }

      let btn = document.getElementsByClassName("btn-show-more")[0];
      btn.style.visibility = "hidden";


    });

    $('.btn').click(function() {
        let btn = document.getElementsByClassName("btn-show-more")[0];
        btn.style.visibility = "visible";

        $(this).addClass('active').siblings().removeClass("active");
        const imageType = $(this).data("filter");
        //console.log(imageType);
        $(".item").hide();

        $(`.item${imageType}`).slice(0,12).show();

    });
});
// /*подключение карусели*/
// $(document).ready(function(){
//     $('.your-class').slick({
//         // setting-name: setting-value
// });
// });
//
// $('.slider-for').slick({
//     slidesToShow: 1,
//     slidesToScroll: 1,
//     arrows: false,
//     fade: true,
//     asNavFor: '.slider-nav'
// });
// $('.slider-nav').slick({
//     slidesToShow: 3,
//     slidesToScroll: 1,
//     asNavFor: '.slider-for',
//     dots: true,
//     centerMode: true,
//     focusOnSelect: true
// });
